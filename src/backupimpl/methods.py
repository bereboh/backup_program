'''
Created on Oct 11, 2012

@author: jean
'''
import os, shutil, SHA1, pickle

exclusions = [ "_svn", "_vti", ".dcu", "/Apps", "/Teaching", "/Config"]

def executesave(data):
    outfile   = open('myArchive\\index.txt','w');
    for root, dir, files in os.walk(data):
        for name in files:
            fullname = root + os.sep + name
            source = fullname
            destination = 'myArchive\\objects\\'+name
            shutil.copyfile(source,destination)
            oldname = destination
            sig = str(SHA1.createFileSignature(fullname))
            newname = 'myArchive\\objects\\'+sig
            os.rename(oldname,newname)
            dict = {fullname:sig}
            pickle.dump(dict,outfile)
            
#    outfile.write(dict)
    outfile.close()

def executelist(foldername):
    for root, files in os.walk("data"):
        for name in files:
            fullname = root + os.sep + file
            source = fullname
            destination = 'myArchive/object'
            shutil.copyfile(source,destination)
            newfile = destination + os.sep + file
            sig = 'SHA1'.createsignature(newfile)
            os.rename(newfile,sig)
            
    outfile   = open("Filelist.txt","r");
    dict = {fullname:sig}
    outfile.read(dict)
    outfile.close()
    
def executerecover(foldername):
    for root, files in os.walk("data"):
        for name in files:
            fullname = root + os.sep + file
            source = fullname
            destination = 'myArchive/object'
            shutil.copyfile(source,destination)
            newfile = destination + os.sep + file
            sig = 'SHA1'.createsignature(newfile)
            os.rename(newfile,sig)
            
    outfile   = open("Filelist.txt","l");
    dict = {fullname:sig}
    outfile.load(dict)
    outfile.close()
    
    