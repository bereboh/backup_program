import os, os.path, hashlib, time, methods

reply = raw_input("Enter command:  ")
print "you entered ", reply
command = reply.split(" ")

if command[1] == 'save':
    methods.executesave(command[2])

elif command[1] == 'list':
    methods.executelist(command[2])

elif command[1] == 'recover':
    methods.executerecover(command[2])
